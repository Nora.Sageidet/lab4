package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    int cols; 
    int rows;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.cols = columns;
        this.rows = rows;
        grid = new CellState[rows][cols];
        for (int i = 0; i < rows; i++) {
            Arrays.fill(this.grid[i], initialState);
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.grid.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < 0 || row > numRows() || column < 0 || column > numColumns()){
            throw new IndexOutOfBoundsException("The number of rows or columns sent to set() is out of bounds.");
        } else {
            this.grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < 0 || row > numRows() || column < 0 || column > numColumns()){
            throw new IndexOutOfBoundsException("The number of rows or columns sent to get() is out of bounds.");
        } else {
            return this.grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int i = 0; i < this.rows; i++){
            for (int j = 0; j < this.cols; j++){
                newGrid.set(i,j,this.get(i,j));
            }
        }
        return newGrid;
        }
    }
