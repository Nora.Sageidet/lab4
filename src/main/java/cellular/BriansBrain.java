package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int cols){
        currentGeneration = new CellGrid(rows, cols, CellState.DEAD);
    }

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return this.currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
		for (int r = 0; r < this.numberOfRows(); r++){
			for (int c = 0; c < this.numberOfColumns(); c++){
				nextGeneration.set(r, c, this.getNextCell(r,c));
			}
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
		CellState state = this.currentGeneration.get(row, col);
        int numAlive = this.countNeighbors(row,col,CellState.ALIVE);

		if (state.equals(CellState.ALIVE)){
			return CellState.DYING;
		} else if (state.equals(CellState.DYING)){
			return CellState.DEAD;
		} else if (state.equals(CellState.DEAD) &&  numAlive == 2){
            return CellState.ALIVE;
        } else {
			return CellState.DEAD;
		}
    }

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int numberAlive = 0;
		int numberDead = 0;

		for (int r = row-1; r <= row+1; r++){
			for (int c = col-1; c <= col+1; c++){
				if (r == row && c == col){
					continue;
				} else if (r < 0 || c < 0 || r >= this.numberOfRows() || c >= this.numberOfColumns()){
					continue;
				} else {
					if (this.currentGeneration.get(r,c).equals(CellState.ALIVE)){
						numberAlive += 1;
					} else {
						numberDead += 1;
					}
				}
					
			}
		}
		if (state.equals(CellState.ALIVE)){
			return numberAlive;
		} else {
			return numberDead;
		}
	}

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return this.currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return this.currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return this.currentGeneration;
    }
    
}
